const mongoose = require('mongoose');

var UserSchema = new mongoose.Schema({
    username: { type: String, lowercase: true, required: [true, "can't be blank"], match: [/^[a-zA-Z0-9]+$/, 'is invalid'] },
    email: { type: String, lowercase: true, required: [true, "can't be blank"] },
    bio: { type: String, default: "Cet utilisateur n'a pas de bio" },
});

const UserModel = mongoose.model('Users', UserSchema);
module.exports = UserModel;